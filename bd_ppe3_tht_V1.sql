-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 02 Juillet 2019 à 10:43
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `bd_ppe3_tht`
--

-- --------------------------------------------------------

--
-- Structure de la table `association`
--

CREATE TABLE IF NOT EXISTS `association` (
  `IDASS` int(11) NOT NULL AUTO_INCREMENT,
  `NOMASS` varchar(50) NOT NULL,
  `LOGOASS` char(50) DEFAULT NULL,
  `CAUSEASS` varchar(1000) NOT NULL,
  PRIMARY KEY (`IDASS`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `association`
--

INSERT INTO `association` (`IDASS`, `NOMASS`, `LOGOASS`, `CAUSEASS`) VALUES
(1, 'Les restos du coeur', NULL, 'Fondés par Coluche en 1985, les Restos du Cœur est une association loi de 1901, reconnue d’utilité publique, sous le nom officiel de « les Restaurants du Cœur – les Relais du Cœur ». Ils ont pour but « d’aider et d’apporter une assistance bénévole aux personnes démunies, notamment dans le domaine alimentaire par l’accès à des repas gratuits, et par la participation à leur insertion sociale et économique, ainsi qu’à toute action contre la pauvreté sous toutes ses formes ».'),
(2, 'la Ligue contre le cancer', NULL, 'Créée en 1918, la Ligue contre le cancer est une association loi 1901 reconnue d’utilité publique reposant sur la générosité du public et sur l’engagement de ses bénévoles et salariés formés grâce à une école de formation agréée pour répondre aux besoins des personnes concernées par le cancer. Notre fédération, composée de 103 Comités départementaux présents sur tout le territoire national, est apolitique et indépendante financièrement'),
(3, 'Zéro Déchet Touraine', NULL, 'Association loi 1901, JO du 11/02/2017\r\nNotre objectif\r\nTendre vers une société zéro déchet\r\n\r\nNos missions\r\nInformer sur la problématique des déchets, inciter et sensibiliser à la prévention et la réduction des déchets, lutter contre l’enfouissement et l’incinération à outrance, proposer des solutions, soutenir toute initiative ou projet en faveur d’une société zéro déchet');

-- --------------------------------------------------------

--
-- Structure de la table `coureur`
--

CREATE TABLE IF NOT EXISTS `coureur` (
  `IDCOU` int(11) NOT NULL,
  `IDEQU` int(11) NOT NULL,
  `DATENAISCOU` date DEFAULT NULL,
  `CERTIFICATMEDCOU` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`IDCOU`),
  KEY `I_FK_COUREUR_EQUIPE` (`IDEQU`),
  KEY `I_FK_COUREUR_PERSONNE` (`IDCOU`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `coureur`
--

INSERT INTO `coureur` (`IDCOU`, `IDEQU`, `DATENAISCOU`, `CERTIFICATMEDCOU`) VALUES
(1, 1, '1981-01-04', 1),
(2, 1, '1981-04-04', 1),
(3, 1, '1981-06-06', 0),
(4, 2, '1990-03-30', NULL),
(5, 2, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

CREATE TABLE IF NOT EXISTS `entreprise` (
  `IDENT` int(11) NOT NULL AUTO_INCREMENT,
  `RAISONSOCIALEENT` varchar(50) DEFAULT NULL,
  `ACTIVITEENT` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`IDENT`),
  UNIQUE KEY `RAISONSOCIALEENT` (`RAISONSOCIALEENT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `equipe`
--

CREATE TABLE IF NOT EXISTS `equipe` (
  `IDEQU` int(11) NOT NULL AUTO_INCREMENT,
  `NOMEQU` varchar(50) DEFAULT NULL,
  `SLOGANEQU` varchar(200) DEFAULT NULL,
  `IDASS` int(11) NOT NULL,
  `AU_DEPART` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`IDEQU`),
  KEY `IDASS` (`IDASS`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `equipe`
--

INSERT INTO `equipe` (`IDEQU`, `NOMEQU`, `SLOGANEQU`, `IDASS`, `AU_DEPART`) VALUES
(1, 'CHEV_PROFS', 'Ne jamais abandonner !', 3, 0),
(2, 'CopCopines', 'Copains se serrent les coudes !', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `licence`
--

CREATE TABLE IF NOT EXISTS `licence` (
  `NUMLIC` varchar(50) NOT NULL,
  `IDPER` int(11) NOT NULL,
  `SPORTLIC` varchar(50) DEFAULT NULL,
  `CLUBLIC` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`NUMLIC`),
  UNIQUE KEY `I_FK_LICENCE_COUREUR` (`IDPER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `licence`
--

INSERT INTO `licence` (`NUMLIC`, `IDPER`, `SPORTLIC`, `CLUBLIC`) VALUES
('5653678', 1, 'fitness', 'SCB');

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

CREATE TABLE IF NOT EXISTS `personne` (
  `IDPER` int(11) NOT NULL AUTO_INCREMENT,
  `NOMPER` varchar(50) DEFAULT NULL,
  `PRENOMPER` varchar(50) DEFAULT NULL,
  `EMAILPER` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`IDPER`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `personne`
--

INSERT INTO `personne` (`IDPER`, `NOMPER`, `PRENOMPER`, `EMAILPER`) VALUES
(1, 'de ROBIEN', 'Frédérique', 'fderobien@gmail.com'),
(2, 'AUTRET', 'Carine', 'carine.autret@gmail.com'),
(3, 'TAMISIER', 'Magali', 'Mt@gmail.com'),
(4, 'DEFRES', 'Maxime', 'max.defres@gmail.com'),
(5, 'AFOND', 'Amandine', 'a.afond@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `pointsrelais`
--

CREATE TABLE IF NOT EXISTS `pointsrelais` (
  `IDPR` int(11) NOT NULL AUTO_INCREMENT,
  `NOMPR` varchar(30) NOT NULL,
  `ADRESSEPR` varchar(100) DEFAULT NULL,
  `VILLEPR` varchar(70) NOT NULL,
  PRIMARY KEY (`IDPR`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `pointsrelais`
--

INSERT INTO `pointsrelais` (`IDPR`, `NOMPR`, `ADRESSEPR`, `VILLEPR`) VALUES
(1, 'Départ Avallon', 'Gymnase', 'Avallon'),
(2, 'PC2', NULL, 'Saint Brancher'),
(3, 'PC3', 'Eglise', 'Saint Léger'),
(4, 'PC4', 'Ecole publique', 'Saint Agnan'),
(5, 'PC5', 'Gymnase', 'Quarré Les Tombes'),
(6, 'PC6', 'Place centrale', 'Saint Germain des champs'),
(7, 'PC7', 'Rue Droite', 'Saint Père'),
(8, 'PC8', 'Place de l''Eglise', 'Vault de Lugny'),
(9, 'Arrivée Avallon', 'Centre Ville', 'Avallon');

-- --------------------------------------------------------

--
-- Structure de la table `sponsor`
--

CREATE TABLE IF NOT EXISTS `sponsor` (
  `IDSPO` int(11) NOT NULL,
  `IDENT` int(11) DEFAULT NULL,
  PRIMARY KEY (`IDSPO`),
  KEY `I_FK_SPONSOR_ENTREPRISE` (`IDENT`),
  KEY `I_FK_SPONSOR_PERSONNE` (`IDSPO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sponsoriser`
--

CREATE TABLE IF NOT EXISTS `sponsoriser` (
  `IDPER` int(11) NOT NULL,
  `IDEQU` int(11) NOT NULL,
  `MONTANTSPONSOR` float DEFAULT NULL,
  PRIMARY KEY (`IDPER`,`IDEQU`),
  KEY `I_FK_SPONSORISER_SPONSOR` (`IDPER`),
  KEY `I_FK_SPONSORISER_EQUIPE` (`IDEQU`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `coureur`
--
ALTER TABLE `coureur`
  ADD CONSTRAINT `coureur_ibfk_1` FOREIGN KEY (`IDEQU`) REFERENCES `equipe` (`IDEQU`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `coureur_ibfk_2` FOREIGN KEY (`IDCOU`) REFERENCES `personne` (`IDPER`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `equipe`
--
ALTER TABLE `equipe`
  ADD CONSTRAINT `equipe_ibfk_1` FOREIGN KEY (`IDASS`) REFERENCES `association` (`IDASS`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `licence`
--
ALTER TABLE `licence`
  ADD CONSTRAINT `licence_ibfk_1` FOREIGN KEY (`IDPER`) REFERENCES `coureur` (`IDCOU`);

--
-- Contraintes pour la table `sponsor`
--
ALTER TABLE `sponsor`
  ADD CONSTRAINT `sponsor_ibfk_1` FOREIGN KEY (`IDENT`) REFERENCES `entreprise` (`IDENT`),
  ADD CONSTRAINT `sponsor_ibfk_2` FOREIGN KEY (`IDSPO`) REFERENCES `personne` (`IDPER`);

--
-- Contraintes pour la table `sponsoriser`
--
ALTER TABLE `sponsoriser`
  ADD CONSTRAINT `sponsoriser_ibfk_1` FOREIGN KEY (`IDPER`) REFERENCES `sponsor` (`IDSPO`),
  ADD CONSTRAINT `sponsoriser_ibfk_2` FOREIGN KEY (`IDEQU`) REFERENCES `equipe` (`IDEQU`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
