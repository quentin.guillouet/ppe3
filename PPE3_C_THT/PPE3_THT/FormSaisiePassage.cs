﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PPE3_THT
{
    public partial class FormSaisiePassage : Form
    {
        private BindingSource bindingSource1 = new BindingSource();
        public FormSaisiePassage()
        {
            InitializeComponent();
        }

        private void FormSaisiePassage_Load(object sender, EventArgs e)
        {
            CBEquipe1.Items.Clear();
            List<KeyValuePair<int, string>> uList = new List<KeyValuePair<int, string>>();
            uList.Add(new KeyValuePair<int, string>(0, "toutes les équipes"));
            CBEquipe1.Items.Add("toutes les équipes");
            for (int i = 0; i < Controleur.Vmodele.Dv_equipes.ToTable().Rows.Count; i++)
            {
                uList.Add(new KeyValuePair<int, string>((int)Controleur.Vmodele.Dv_equipes.ToTable().Rows[i][0], Controleur.Vmodele.Dv_equipes.ToTable().Rows[i][1].ToString()));
            }
            CBEquipe1.DataSource = uList;
            CBEquipe1.ValueMember = "Key";
            CBEquipe1.DisplayMember = "Value";
            CBEquipe1.Text = CBEquipe1.Items[0].ToString();

            CBEquipe1.DropDownStyle = ComboBoxStyle.DropDownList;

            CBPointRelais1.Items.Clear();
            List<KeyValuePair<int, string>> uList1 = new List<KeyValuePair<int, string>>();
            uList1.Add(new KeyValuePair<int, string>(0, "toutes les équipes"));
            CBPointRelais1.Items.Add("toutes les équipes");
            for (int i = 0; i < Controleur.Vmodele.Dv_relais.ToTable().Rows.Count; i++)
            {
                uList1.Add(new KeyValuePair<int, string>((int)Controleur.Vmodele.Dv_relais.ToTable().Rows[i][0], Controleur.Vmodele.Dv_relais.ToTable().Rows[i][1].ToString()));
            }
            CBPointRelais1.DataSource = uList1;
            CBPointRelais1.ValueMember = "Key";
            CBPointRelais1.DisplayMember = "Value";
            CBPointRelais1.Text = CBPointRelais1.Items[0].ToString();

            CBPointRelais1.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            Controleur.crud_passage(this);
        }

        private void BtnAnn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
