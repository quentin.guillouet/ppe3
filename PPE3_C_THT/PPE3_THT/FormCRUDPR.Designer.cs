﻿using System.Windows.Forms;

namespace PPE3_THT
{
    partial class FormCRUDPR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.BtnOK = new System.Windows.Forms.Button();
            this.BtnAnnuler = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.tbNom = new System.Windows.Forms.MaskedTextBox();
            this.tbAdresse = new System.Windows.Forms.MaskedTextBox();
            this.tbLong = new System.Windows.Forms.MaskedTextBox();
            this.tbLat = new System.Windows.Forms.MaskedTextBox();
            this.tbPoint = new System.Windows.Forms.MaskedTextBox();
            this.tbVille = new System.Windows.Forms.MaskedTextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nom du point relais :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Adresse du point relais :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(32, 175);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ville du point relais :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(85, 229);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Longitude :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(97, 286);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Latitude :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(38, 340);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = "Point kilométrique :";
            // 
            // BtnOK
            // 
            this.BtnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.BtnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnOK.Location = new System.Drawing.Point(83, 401);
            this.BtnOK.Name = "BtnOK";
            this.BtnOK.Size = new System.Drawing.Size(88, 32);
            this.BtnOK.TabIndex = 12;
            this.BtnOK.Text = "Ok";
            this.BtnOK.UseVisualStyleBackColor = true;
            // 
            // BtnAnnuler
            // 
            this.BtnAnnuler.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BtnAnnuler.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAnnuler.Location = new System.Drawing.Point(283, 401);
            this.BtnAnnuler.Name = "BtnAnnuler";
            this.BtnAnnuler.Size = new System.Drawing.Size(82, 32);
            this.BtnAnnuler.TabIndex = 13;
            this.BtnAnnuler.Text = "Annuler";
            this.BtnAnnuler.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(158, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(137, 24);
            this.label7.TabIndex = 14;
            this.label7.Text = "POINT RELAIS";
            // 
            // tbNom
            // 
            this.tbNom.Location = new System.Drawing.Point(176, 65);
            this.tbNom.Name = "tbNom";
            this.tbNom.Size = new System.Drawing.Size(213, 20);
            this.tbNom.TabIndex = 15;
            this.tbNom.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.TbNom_MaskInputRejected);
            // 
            // tbAdresse
            // 
            this.tbAdresse.Location = new System.Drawing.Point(176, 121);
            this.tbAdresse.Name = "tbAdresse";
            this.tbAdresse.Size = new System.Drawing.Size(213, 20);
            this.tbAdresse.TabIndex = 16;
            // 
            // tbLong
            // 
            this.tbLong.Location = new System.Drawing.Point(176, 228);
            this.tbLong.Mask = "#999.#99999999999999999";
            this.tbLong.Name = "tbLong";
            this.tbLong.Size = new System.Drawing.Size(213, 20);
            this.tbLong.TabIndex = 17;
            // 
            // tbLat
            // 
            this.tbLat.Location = new System.Drawing.Point(176, 285);
            this.tbLat.Mask = "#999.#99999999999999999";
            this.tbLat.Name = "tbLat";
            this.tbLat.Size = new System.Drawing.Size(213, 20);
            this.tbLat.TabIndex = 18;
            // 
            // tbPoint
            // 
            this.tbPoint.Location = new System.Drawing.Point(176, 339);
            this.tbPoint.Mask = "#999.#99";
            this.tbPoint.Name = "tbPoint";
            this.tbPoint.Size = new System.Drawing.Size(213, 20);
            this.tbPoint.TabIndex = 19;
            // 
            // tbVille
            // 
            this.tbVille.Location = new System.Drawing.Point(176, 174);
            this.tbVille.Mask = ">L<????????????????????????????????";
            this.tbVille.Name = "tbVille";
            this.tbVille.Size = new System.Drawing.Size(213, 20);
            this.tbVille.TabIndex = 20;
            // 
            // FormCRUDPR
            // 
            this.AcceptButton = this.BtnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.BtnAnnuler;
            this.ClientSize = new System.Drawing.Size(476, 461);
            this.ControlBox = false;
            this.Controls.Add(this.tbVille);
            this.Controls.Add(this.tbPoint);
            this.Controls.Add(this.tbLat);
            this.Controls.Add(this.tbLong);
            this.Controls.Add(this.tbAdresse);
            this.Controls.Add(this.tbNom);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.BtnAnnuler);
            this.Controls.Add(this.BtnOK);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FormCRUDPR";
            this.Text = "FormCRUDPR";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button BtnOK;
        private System.Windows.Forms.Button BtnAnnuler;
        private System.Windows.Forms.Label label7;
        private MaskedTextBox tbNom;
        private MaskedTextBox tbAdresse;
        private MaskedTextBox tbLong;
        private MaskedTextBox tbLat;
        private MaskedTextBox tbPoint;
        private MaskedTextBox tbVille;

        public MaskedTextBox TbNom { get => tbNom; set => tbNom = value; }
        public MaskedTextBox TbAdresse { get => tbAdresse; set => tbAdresse = value; }
        public MaskedTextBox TbLong { get => tbLong; set => tbLong = value; }
        public MaskedTextBox TbLat { get => tbLat; set => tbLat = value; }
        public MaskedTextBox TbPoint { get => tbPoint; set => tbPoint = value; }
        public MaskedTextBox TbVille { get => tbVille; set => tbVille = value; }
    }
}