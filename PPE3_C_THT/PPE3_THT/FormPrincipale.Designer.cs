﻿namespace PPE3_THT
{
    partial class FormPrincipale
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.modeConnectéToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionDesPointsRelaisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exporterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modeDéconnectéToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.affichageDesCoureursEtÉquipesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionDesPassagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.classementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qUITTERToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modeConnectéToolStripMenuItem,
            this.modeDéconnectéToolStripMenuItem,
            this.qUITTERToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(912, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // modeConnectéToolStripMenuItem
            // 
            this.modeConnectéToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gestionDesPointsRelaisToolStripMenuItem,
            this.importerToolStripMenuItem,
            this.exporterToolStripMenuItem});
            this.modeConnectéToolStripMenuItem.Name = "modeConnectéToolStripMenuItem";
            this.modeConnectéToolStripMenuItem.Size = new System.Drawing.Size(104, 20);
            this.modeConnectéToolStripMenuItem.Text = "Mode Connecté";
            this.modeConnectéToolStripMenuItem.Click += new System.EventHandler(this.modeConnectéToolStripMenuItem_Click);
            // 
            // gestionDesPointsRelaisToolStripMenuItem
            // 
            this.gestionDesPointsRelaisToolStripMenuItem.Name = "gestionDesPointsRelaisToolStripMenuItem";
            this.gestionDesPointsRelaisToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.gestionDesPointsRelaisToolStripMenuItem.Text = "Gestion des Points Relais";
            this.gestionDesPointsRelaisToolStripMenuItem.Click += new System.EventHandler(this.gestionDesPointsRelaisToolStripMenuItem_Click);
            // 
            // importerToolStripMenuItem
            // 
            this.importerToolStripMenuItem.Name = "importerToolStripMenuItem";
            this.importerToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.importerToolStripMenuItem.Text = "Importer";
            this.importerToolStripMenuItem.Click += new System.EventHandler(this.importerToolStripMenuItem1_Click);
            // 
            // exporterToolStripMenuItem
            // 
            this.exporterToolStripMenuItem.Name = "exporterToolStripMenuItem";
            this.exporterToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.exporterToolStripMenuItem.Text = "Exporter";
            this.exporterToolStripMenuItem.Click += new System.EventHandler(this.exporterToolStripMenuItem1_Click);
            // 
            // modeDéconnectéToolStripMenuItem
            // 
            this.modeDéconnectéToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.affichageDesCoureursEtÉquipesToolStripMenuItem,
            this.gestionDesPassagesToolStripMenuItem,
            this.classementToolStripMenuItem});
            this.modeDéconnectéToolStripMenuItem.Name = "modeDéconnectéToolStripMenuItem";
            this.modeDéconnectéToolStripMenuItem.Size = new System.Drawing.Size(116, 20);
            this.modeDéconnectéToolStripMenuItem.Text = "Mode Déconnecté";
            // 
            // affichageDesCoureursEtÉquipesToolStripMenuItem
            // 
            this.affichageDesCoureursEtÉquipesToolStripMenuItem.Name = "affichageDesCoureursEtÉquipesToolStripMenuItem";
            this.affichageDesCoureursEtÉquipesToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.affichageDesCoureursEtÉquipesToolStripMenuItem.Text = "Affichage des coureurs et équipes";
            this.affichageDesCoureursEtÉquipesToolStripMenuItem.Click += new System.EventHandler(this.affichageDesCoureursEtÉquipesToolStripMenuItem_Click);
            // 
            // gestionDesPassagesToolStripMenuItem
            // 
            this.gestionDesPassagesToolStripMenuItem.Name = "gestionDesPassagesToolStripMenuItem";
            this.gestionDesPassagesToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.gestionDesPassagesToolStripMenuItem.Text = "Saisie des passages";
            this.gestionDesPassagesToolStripMenuItem.Click += new System.EventHandler(this.GestionDesPassagesToolStripMenuItem_Click);
            // 
            // classementToolStripMenuItem
            // 
            this.classementToolStripMenuItem.Name = "classementToolStripMenuItem";
            this.classementToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.classementToolStripMenuItem.Text = "Classement";
            // 
            // qUITTERToolStripMenuItem
            // 
            this.qUITTERToolStripMenuItem.Name = "qUITTERToolStripMenuItem";
            this.qUITTERToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.qUITTERToolStripMenuItem.Text = "QUITTER";
            this.qUITTERToolStripMenuItem.Click += new System.EventHandler(this.qUITTERToolStripMenuItem_Click);
            // 
            // FormPrincipale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PPE3_THT.Properties.Resources.Capture;
            this.ClientSize = new System.Drawing.Size(912, 329);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormPrincipale";
            this.Text = "Application Terra Human Trail";
            this.Load += new System.EventHandler(this.FormPrincipale_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem modeConnectéToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionDesPointsRelaisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modeDéconnectéToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionDesPassagesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem classementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qUITTERToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exporterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem affichageDesCoureursEtÉquipesToolStripMenuItem;
       
    }
}

