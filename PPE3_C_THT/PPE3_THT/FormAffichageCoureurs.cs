﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PPE3_THT
{
    /// <summary>
    /// Feuille d'affichage des coureurs par équipe en mode déconnecté
    /// Possibilité de mettre au départ une ou plusieurs équipes
    /// </summary>
    public partial class FormAffichageCoureurs : Form
    {
        private BindingSource bindingSource1 = new BindingSource();

        public FormAffichageCoureurs()
        {
            InitializeComponent();
            chargeDonnees();
        }

        /// <summary>
        /// remplit la comboBox avec toutes les équipes et le dataGridView avec tous les coureurs
        /// </summary>
        private void chargeDonnees()
        {
            // comboBox à charger avec les équipes
            cbEquipes.Items.Clear();
            List<KeyValuePair<int, string>> uList = new List<KeyValuePair<int, string>>();
            uList.Add(new KeyValuePair<int, string>(0, "toutes les équipes"));
            cbEquipes.Items.Add("toutes les équipes");
            for (int i = 0; i < Controleur.Vmodele.Dv_equipes.ToTable().Rows.Count; i++)
            {
                uList.Add(new KeyValuePair<int, string>((int)Controleur.Vmodele.Dv_equipes.ToTable().Rows[i][0], Controleur.Vmodele.Dv_equipes.ToTable().Rows[i][1].ToString()));
            }
            cbEquipes.DataSource = uList;
            cbEquipes.ValueMember = "Key";
            cbEquipes.DisplayMember = "Value";
            cbEquipes.Text = cbEquipes.Items[0].ToString();

            cbEquipes.DropDownStyle = ComboBoxStyle.DropDownList;

            // pour afficher tous les coureurs dans le dataGridView
            bindingSource1.DataSource = Controleur.Vmodele.Dv_coureurs;
            dataGVC.DataSource = bindingSource1;
       

            // amélioration de l'affichage du dataGridView
            dataGVC.Columns[0].HeaderText = "ID";
            dataGVC.Columns[1].HeaderText = "NOM";
            dataGVC.Columns[2].HeaderText = "PRENOM";
            dataGVC.Columns[3].HeaderText = "EMAIL";
            dataGVC.Columns[4].HeaderText = "DATE NAISS";
            dataGVC.Columns[5].HeaderText = "CERTIF MED";
            dataGVC.Columns[6].HeaderText = "ID EQU";
            dataGVC.Columns[7].HeaderText = "NOM EQUIPE";


            dataGVC.Refresh();
        }

        /// <summary>
        /// modifie le dataGridView avec les coureurs selon l'équipe sélectionnée dans la comboBox (ou tous)
        /// </summary>
        private void changefiltre()
        {
            string num = cbEquipes.SelectedValue.ToString();

            int n = Convert.ToInt32(num);
            if (n == 0) // cas de "toutes les équipes"
                Controleur.Vmodele.Dv_coureurs.RowFilter = "";
            else
            {
                string Filter = "IdEqu = '" + n + "'";
                Controleur.Vmodele.Dv_coureurs.RowFilter = Filter;
            }
            dataGVC.Refresh();

        }


        private void cbEquipes_SelectionChangeCommitted(object sender, EventArgs e)
        {
            changefiltre();
        }

        private void FormAffichageCoureurs_Load(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// fonction qui retourne l'index dans le dataView de l'équipe sélectionnée dans la comboBox à partir de l'IdEqu passé en paramètre
        /// </summary>
        /// <param name="n">IdEqu</param>
        /// <returns>index de l'équipe dans le dataView</returns>
        private int RechercheEquipe(int n)
        {
            int index = -1;
            for (int i = 0; i < Controleur.Vmodele.Dv_equipes.Count; i++)
            {
                if (n == (int)Controleur.Vmodele.Dv_equipes.ToTable().Rows[i][0])
                {
                    index = i;
                }
            }
            return index;
        }


        /// <summary>
        /// Permet de sélectionner les équipes au départ de la course
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDepart_Click(object sender, EventArgs e)
        {
            // recupère l'IdEqu de l'équipe sélectionnée
            string num = cbEquipes.SelectedValue.ToString();

            int n = Convert.ToInt32(num);


            if (n == 0) // cas de "toutes les équipes"
            {
                // mettre toutes les équipes au départ
                for (int i = 0; i < Controleur.Vmodele.Dv_equipes.Count; i++)
                {
                    Controleur.Vmodele.Dv_equipes[i]["Au_Depart"] = 1;
                }
                MessageBox.Show("Toutes les Equipes sont au départ ! BD mise à jour.");
            }
            else
            {
                // mise à 1 du champ "Au_depart"  via le dataView pour l'équipe sélectionnée
                int index = RechercheEquipe(n);
                if (index != -1)
                {
                    Controleur.Vmodele.Dv_equipes[index]["Au_Depart"] = 1;
                    MessageBox.Show("Equipe : " + Controleur.Vmodele.Dv_equipes.ToTable().Rows[index][1].ToString() + " est au depart " + Controleur.Vmodele.Dv_equipes[index]["Au_Depart"].ToString());
                }
                else
                {
                    MessageBox.Show("Equipe non trouvée");
                }
            }
        }
    }
}
