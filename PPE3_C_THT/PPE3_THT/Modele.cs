﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;
using System.Collections;

namespace PPE3_THT
{
    /// <summary>
    /// Classe MODELE : gestion de la BDD
    /// </summary>
    public class Modele
    {
        #region propriétés

        private MySqlConnection myConnection;   // objet de connexion
        private bool connopen = false;          // test si la connexion est faite
        private bool errgrave = false;          // test si erreur lors de la connexion
        private bool chargement = false;        // test si le chargement d'une requête est fait

        // les DataAdapter et DataTable seront gérés dans des collections avec pour chaque un indice correspondant :
        // indice 0 : récupération des noms des tables
        // indice 1 : Table Constructeur
        // indice 2 : Table Support avec jointure pour récupérer tous les libellés
        // indice 3 : Table Support

        // collection de DataAdapter
        private List<MySqlDataAdapter> dA = new List<MySqlDataAdapter>();

        // collection de DataTable récupérant les données correspond au DA associé
        private List<DataTable> dT = new List<DataTable>();


        private MySqlDataAdapter mySqlDataAdapterImport = new MySqlDataAdapter();
        private DataSet dataSetImport = new DataSet();
        private DataView dv_coureurs = new DataView(), dv_equipes = new DataView(), dv_relais = new DataView(), dv_passage = new DataView();

        private char vaction, vtable;

        private bool errmaj = false;
        private ArrayList rapport = new ArrayList();


        #endregion

        #region accesseurs
        /// <summary>
        /// test si la connexion à la BD est ouverte
        /// </summary>
        public bool Connopen
        {
            get { return connopen; }
            set { connopen = value; }
        }

        /// <summary>
        /// test si erreur lors de la connexion
        /// </summary>
        public bool Errgrave
        {
            get { return errgrave; }
            set { errgrave = value; }
        }

        /// <summary>
        /// test si le chargement d'une requête est fait
        /// </summary>
        public bool Chargement
        {
            get { return chargement; }
            set { chargement = value; }
        }

        /// <summary>
        /// Accesseur de la collection des DataAdapter
        /// </summary>
        public List<MySqlDataAdapter> DA
        {
            get { return dA; }
            set { dA = value; }
        }

        /// <summary>
        /// Accesseur de la collection des DataTable
        /// </summary>
        public List<DataTable> DT
        {
            get { return dT; }
            set { dT = value; }
        }

        /// <summary>
        /// Accesseur du dataView contenant les coureurs en mode déconnecté
        /// </summary>
        public DataView Dv_coureurs
        {
            get
            {
                return dv_coureurs;
            }

            set
            {
                dv_coureurs = value;
            }
        }

        /// <summary>
        /// Accesseur du dataView contenant les équipes en mode déconnecté
        /// </summary>
        public DataView Dv_equipes
        {
            get
            {
                return dv_equipes;
            }

            set
            {
                dv_equipes = value;
            }
        }

        /// <summary>
        /// caractère précisant quelle action on fait en mode déconnecté sur une table : 'u' : update
        /// </summary>
        public char Vaction
        {
            get
            {
                return vaction;
            }

            set
            {
                vaction = value;
            }
        }

        /// <summary>
        /// caractère précisant sur quelle table on travaille en mode déconnecté ; si 'e' : table EQUIPE
        /// </summary>
        public char Vtable
        {
            get
            {
                return vtable;
            }

            set
            {
                vtable = value;
            }
        }

        public DataView Dv_relais { get => dv_relais; set => dv_relais = value; }
        public DataView Dv_passage { get => dv_passage; set => dv_passage = value; }
        public bool Errmaj { get => errmaj; set => errmaj = value; }
        public ArrayList Rapport { get => rapport; set => rapport = value; }


        #endregion

        /// <summary>
        /// Modele() : constructeur permettant l'ajout des DataAdpater et DataTable nécessaires (4 nécessaires pour l'existant actuel)
        /// indice 0 : récupération des noms des tables
        /// indice 1 : Table PointsRelais
        /// indice 2 :
        /// indice 3 : 
        /// </summary>
        public Modele()
        {

            for (int i = 0; i < 3; i++)
            {
                dA.Add(new MySqlDataAdapter());
                dT.Add(new DataTable());
            }
        }

        /// <summary>
        /// méthode seconnecter permettant la connexion à la BD : bd_ppe3_THT
        /// </summary>
        public void seconnecter()
        {
            string myConnectionString = "Database=ppe3;Data Source=192.168.221.1;User Id=root;Password=P@ssw0rd;";
            myConnection = new MySqlConnection(myConnectionString);
            try // tentative 
            {
                myConnection.Open();
                connopen = true;
            }
            catch (Exception err)// gestion des erreurs
            {
                MessageBox.Show("Erreur ouverture BD THT : " + err, "PBS connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                connopen = false; errgrave = true;
            }
        }

        /// <summary>
        /// méthode sedeconnecter pour se déconnecter à la BD
        /// </summary>
        public void sedeconnecter()
        {
            if (!connopen)
                return;
            try
            {
                myConnection.Close();
                myConnection.Dispose();
                connopen = false;
            }
            catch (Exception err)
            {
                MessageBox.Show("Erreur fermeture BD THT : " + err, "PBS deconnection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                errgrave = true;
            }
        }

        /// <summary>
        /// méthode générique privée pour charger le résultat d'une requête dans un dataTable via un dataAdapter
        /// Méthode appelée par charger_donnees(string table)
        /// </summary>
        /// <param name="requete">requete à charger</param>
        /// <param name="DT">dataTable</param>
        /// <param name="DA">dataAdapter</param>
        private void charger(string requete, DataTable DT, MySqlDataAdapter DA)
        {
            DA.SelectCommand = new MySqlCommand(requete, myConnection);

            // pour spécifier les instructions de mise à jour (insert, delete, update)
            MySqlCommandBuilder CB1 = new MySqlCommandBuilder(DA);
            try
            {
                DT.Clear();
                DA.Fill(DT);
                chargement = true;
            }
            catch (Exception err)
            {
                MessageBox.Show("Erreur chargement dataTable : " + err, "PBS table", MessageBoxButtons.OK, MessageBoxIcon.Error);
                errgrave = true;
            }
        }

        /// <summary>
        /// charge dans un DT les données de la table passée en paramètre
        /// </summary>
        /// <param name="table">nom de la table à requêter</param>
        public void charger_donnees(string table)
        {
            chargement = false;
            if (!connopen) return;		// pour vérifier que la BD est bien ouverte

            if (table == "toutes")
            {
                charger("show tables;", dT[0], dA[0]);
            }
            if (table == "pointsrelais")
            {
                charger("select * from pointsrelais;", dT[1], dA[1]);
            }
            if (table == "passage")
            {
                charger("select * from passage;", dT[2], dA[2]);
            }
 

        }

        /// <summary>
        /// Importation des données concernant les coureurs et leur équipe en vue d'une utilisation en mode déconnecté
        /// L'importation se fait dans des dataView, autant de dataView que de requêtes faites
        /// </summary>
        public void import()
        {
            if (!connopen)
                return;
            mySqlDataAdapterImport.SelectCommand = new MySqlCommand("select idcou, nomper, prenomper, emailper, datenaiscou, certificatmedcou, c.idequ, nomequ from coureur c inner join personne p on c.idcou = p.idper inner join equipe e on e.idequ = c.idequ order by c.idcou;select * from equipe;select * from pointsrelais;select * from passage", myConnection);
            MySqlCommandBuilder CB1 = new MySqlCommandBuilder(mySqlDataAdapterImport);
            try
            {
                dataSetImport.Clear();
                mySqlDataAdapterImport.Fill(dataSetImport);
                MySqlCommand vcommand = myConnection.CreateCommand();
         
                // selection de tous les coureurs dans le dataView dv_coureurs
                dv_coureurs = dataSetImport.Tables[0].DefaultView;
                // selection de toutes les équipes dans le dataView dv_equipes
                dv_equipes = dataSetImport.Tables[1].DefaultView;
                // selection de toutes les points relais dans le dataView dv_relais
                dv_relais = dataSetImport.Tables[2].DefaultView;
                // selection de toutes les passages dans le dataView dv_relais
                dv_passage = dataSetImport.Tables[3].DefaultView;

                chargement = true;

            }
            catch (Exception err)
            {
                MessageBox.Show("Erreur chargement dataset : " + err, "PBS coureurs/equipes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                errgrave = true;
            }
        }


        /// <summary>
        /// Exportation des données des dataView pour remettre à jour les données dans la BD
        /// </summary>
        public void export()
        {
            if (!connopen)
                return;
            try
            {
                maj_equipes();
                add_passage();
            }
            catch (Exception err)
            {
                MessageBox.Show("erreur dans l'export maj equipes" + err);
            }
        }

        /// <summary>
        /// Méthode pour mettre à jour les données du dataView dv_equipes (modifié en mode déconnecté) dans la table EQUIPE de la BDD
        /// </summary>
        public void maj_equipes()
        {
            vaction = 'u'; vtable = 'e';
            if (!connopen) return;
                                                                                               
            // préparation de la requête avec les paramètres pour mettre à jour le champ "Au_Depart"
            mySqlDataAdapterImport.UpdateCommand = new MySqlCommand("update equipe set AU_DEPART=?Au_Depart where IdEqu = ?IdEqu", myConnection);// notre commandbuider ici update non fait si maj ou delete dans la base

            // déclaration des paramètres utiles au commandbuilder
            mySqlDataAdapterImport.UpdateCommand.Parameters.Add("?IdEqu", MySqlDbType.Int32, 10, "IDEQU");
            mySqlDataAdapterImport.UpdateCommand.Parameters.Add("?Au_Depart", MySqlDbType.Int16, 4, "AU_DEPART");


            //on continue même si erreur de MAJ
            mySqlDataAdapterImport.ContinueUpdateOnError = true;

            //table concernée 1 = equipe
            DataTable table = dataSetImport.Tables[3];
            //on ne s'occupe que des enregistrements modifiés en local dans la table du dataView concernée
            mySqlDataAdapterImport.Update(table.Select(null, null, DataViewRowState.ModifiedCurrent));
        }
        private void OnRowUpdated(object sender, MySqlRowUpdatedEventArgs args)
        // utile pour ajout,modif,supp
        {
            string msg = "";
            Int64 nb = 0;
            if (args.Status == UpdateStatus.ErrorsOccurred)
            {
                if (vaction == 'd' || vaction == 'u')
                {
                    MySqlCommand vcommand = myConnection.CreateCommand();
                    if (vtable == 'p') // ‘p’ pour table PASSAGE
                    {
                        vcommand.CommandText = "SELECT COUNT(*) FROM passage WHERE IDEQUIPE = '" + args.Row[0, DataRowVersion.Original] + "' AND IDPR = '" + args.Row[1, DataRowVersion.Original] + "';";
                    }
                    nb = (Int64)vcommand.ExecuteScalar();
                    // on veut savoir si la personne existe encore dans la base
                }
                if (vaction == 'd')
                {
                    if (nb == 1) // pour delete si l'enr a été supprimé on n'affiche pas l'erreur
                    {
                        if (vtable == 'p')
                        {
                            msg = "pour le numéro de passage : " + args.Row[0, DataRowVersion.Original] + "impossible delete car enr modifié dans la base";
                        }
                        rapport.Add(msg);
                        errmaj = true;
                    }
                }
                if (vaction == 'u')
                {
                    if (nb == 1)
                    {
                        if (vtable == 'p')
                        {
                            msg = "pour le numéro de passage: " + args.Row[0, DataRowVersion.Original] + "impossible MAJ car enr modifié dans la base";
                        }
                        rapport.Add(msg);
                        errmaj = true;
                    }
                    else
                    {
                        if (vtable == 'p')
                        {
                            msg = "pour le numéro de passage : " + args.Row[0, DataRowVersion.Original] + "impossible MAJ car enr supprimé dans la base";
                        }
                        rapport.Add(msg);
                        errmaj = true;
                    }
                }
                if (vaction == 'c')
                {
                    if (vtable == 'p')
                    {
                        msg = "pour le numéro de passage : " + args.Row[0, DataRowVersion.Current] + "impossible ADD car erreur données";
                    }
                    rapport.Add(msg);
                    errmaj = true;
                }
            }
        }

        public void add_passage()
        {
            vaction = 'c'; // on précise bien l’action, ici c pour create
            vtable = 'p';
            if (!connopen) return;
            //appel d'une méthode sur l'événement ajout d'un enr de la table
            mySqlDataAdapterImport.RowUpdated += new MySqlRowUpdatedEventHandler(OnRowUpdated);
            mySqlDataAdapterImport.InsertCommand = new MySqlCommand("insert into passage (`IDEQUIPE`,`IDPR`,`JOURETHEURE`) values (?idequ,?idpr,?date)", myConnection);// notre commandbuilder ici ajout non fait si erreur de données
            //déclaration des variables utiles au commandbuilder
            // pas besoin de créer l’IdPersonne car en auto-increment
            mySqlDataAdapterImport.InsertCommand.Parameters.Add("?idequ", MySqlDbType.Int16, 10, "IDEQUIPE");
            mySqlDataAdapterImport.InsertCommand.Parameters.Add("?idpr", MySqlDbType.Int16, 10, "IDPR");
            mySqlDataAdapterImport.InsertCommand.Parameters.Add("?date", MySqlDbType.DateTime, 50, "JOURETHEURE");
            //on continue même si erreur de MAJ
            mySqlDataAdapterImport.ContinueUpdateOnError = true;
            //table concernée 1 = personne
            DataTable table = dataSetImport.Tables[3];
            //on ne s'occupe que des enregistrement ajoutés en local
            mySqlDataAdapterImport.Update(table.Select(null, null, DataViewRowState.Added));
            //désassocie la méthode sur l'événement maj de la base
            mySqlDataAdapterImport.RowUpdated -= new MySqlRowUpdatedEventHandler(OnRowUpdated);
        }
    }
}
