﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PPE3_THT
{
    /// <summary>
    /// Feuille de gestion des points relais en mode connecté avec chargement de tous les points relais dans un dataGridView en lecture seule
    /// </summary>
    public partial class FormGestionPR : Form
    {
        private BindingSource bS1 = new BindingSource();
        public FormGestionPR()
        {
            InitializeComponent();
        }

        private void FormGestionPR_Load(object sender, EventArgs e)
        {
            Controleur.init();
            Controleur.Vmodele.seconnecter();
            if (Controleur.Vmodele.Connopen == false)
            {
                MessageBox.Show("Erreur dans la connexion");
            }
            else
            {

                Controleur.Vmodele.charger_donnees("pointsrelais");      // chargement des données de la table POINTSRELAIS
                if (Controleur.Vmodele.Chargement)
                {
                    // un DT par table
                    bS1 = new BindingSource();
                 
                    bS1.DataSource = Controleur.Vmodele.DT[1];
                    dataGVPR.DataSource = bS1;
                    dataGVPR.Columns[0].HeaderText = "IDENTIFIANT";
                    dataGVPR.Columns[1].HeaderText = "NOM";
                    dataGVPR.Columns[2].HeaderText = "ADRESSE";
                    dataGVPR.Columns[3].HeaderText = "VILLE";


                    // mise à jour du dataGridView via le bindingSource rempli par le DataTable
                    dataGVPR.Refresh();
                    dataGVPR.Visible = true;
                }




            }
        }

        private void FormGestionPR_FormClosed(object sender, FormClosedEventArgs e)
        {
            Controleur.Deconnexion();
        }

        private void DataGVPR_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void AjouterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // vérifier qu’une ligne est bien sélectionnée dans le dataGridView
            if (dataGVPR.SelectedRows.Count == 1)
            {
                // appel de la méthode du controleur en mode update et avec la valeur de l’IdPersonne en clé
                Controleur.crud_relais('c', -1);
                // mise à jour du dataGridView en affichage
                bS1.MoveLast();
                bS1.MoveFirst();
                dataGVPR.Refresh();
            }
            else
            {
                MessageBox.Show("Sélectionner une ligne à modifier");
            }
        }

        private void ModifierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // vérifier qu’une ligne est bien sélectionnée dans le dataGridView
            if (dataGVPR.SelectedRows.Count == 1)
            {
                // appel de la méthode du controleur en mode update et avec la valeur de l’IdPersonne en clé
                Controleur.crud_relais('u', Convert.ToInt32(dataGVPR.SelectedRows[0].Index));
                // mise à jour du dataGridView en affichage
                bS1.MoveLast();
                bS1.MoveFirst();
                dataGVPR.Refresh();
            }
            else
            {
                MessageBox.Show("Sélectionner une ligne à modifier");
            }
        }

        private void SupprimerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // vérifier qu’une ligne est bien sélectionnée dans le dataGridView
            if (dataGVPR.SelectedRows.Count == 1)
            {
                DialogResult Result = MessageBox.Show("Voulez-vous vraiment supprimer ?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if(Result == DialogResult.Yes)
                {
                    Controleur.Vmodele.DT[1].Rows[Convert.ToInt32(dataGVPR.SelectedRows[0].Index)].Delete();
                    Controleur.Vmodele.DA[1].Update(Controleur.Vmodele.DT[1]);
                    bS1.MoveLast();
                    bS1.MoveFirst();
                    dataGVPR.Refresh();
                }
            }
            else
            {
                MessageBox.Show("Sélectionner une ligne à modifier");
            }
        }
    }
}
