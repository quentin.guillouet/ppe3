﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PPE3_THT
{
    /// <summary>
    /// Feuille principale de l'application avec la gestion du menu
    /// </summary>
    public partial class FormPrincipale : Form
    {
        public FormPrincipale()
        {
            InitializeComponent();
        }




    


        private void qUITTERToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void modeConnectéToolStripMenuItem_Click(object sender, EventArgs e)
        {
       
        }

        private void gestionDesPointsRelaisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormGestionPR PR = new FormGestionPR();
            PR.Show();
        }

        private void importerToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Controleur.init();

            Controleur.Vmodele.seconnecter();
            if (Controleur.Vmodele.Connopen == false)
            {
                MessageBox.Show("Erreur dans la connexion");
            }
            else
            {
                //MessageBox.Show("BD connectée");
                Controleur.Vmodele.import();
                if (Controleur.Vmodele.Chargement)
                {
                    modeDéconnectéToolStripMenuItem.Enabled = true;
                    exporterToolStripMenuItem.Enabled = true;
                    importerToolStripMenuItem.Enabled = false;
                }
                else
                    MessageBox.Show("Erreur dans l'Importation des données");

            }
            Controleur.Vmodele.sedeconnecter();
        }

        private void FormPrincipale_Load(object sender, EventArgs e)
        {
            modeDéconnectéToolStripMenuItem.Enabled = false;
            
            exporterToolStripMenuItem.Enabled = false;
        }

        private void affichageDesCoureursEtÉquipesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAffichageCoureurs AC = new FormAffichageCoureurs();
            AC.Show();
        }

        private void exporterToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Controleur.Vmodele.seconnecter();
            if (Controleur.Vmodele.Connopen == false)
            {
                MessageBox.Show("Erreur dans la connexion");
            }
            else
            {
                Controleur.Vmodele.export();
                modeDéconnectéToolStripMenuItem.Enabled = false;
                importerToolStripMenuItem.Enabled = true;
                exporterToolStripMenuItem.Enabled = false;
            }
            Controleur.Vmodele.sedeconnecter();
        }

        private void GestionDesPassagesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormSaisiePassage FSP = new FormSaisiePassage();
            FSP.Show();
        }
    }
}
