﻿namespace PPE3_THT
{
    partial class FormAffichageCoureurs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbEquipes = new System.Windows.Forms.ComboBox();
            this.dataGVC = new System.Windows.Forms.DataGridView();
            this.btnDepart = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGVC)).BeginInit();
            this.SuspendLayout();
            // 
            // cbEquipes
            // 
            this.cbEquipes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEquipes.FormattingEnabled = true;
            this.cbEquipes.Location = new System.Drawing.Point(197, 24);
            this.cbEquipes.Name = "cbEquipes";
            this.cbEquipes.Size = new System.Drawing.Size(248, 21);
            this.cbEquipes.TabIndex = 0;
            this.cbEquipes.SelectionChangeCommitted += new System.EventHandler(this.cbEquipes_SelectionChangeCommitted);
            // 
            // dataGVC
            // 
            this.dataGVC.AllowUserToAddRows = false;
            this.dataGVC.AllowUserToDeleteRows = false;
            this.dataGVC.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGVC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGVC.Location = new System.Drawing.Point(12, 77);
            this.dataGVC.Name = "dataGVC";
            this.dataGVC.ReadOnly = true;
            this.dataGVC.Size = new System.Drawing.Size(608, 150);
            this.dataGVC.TabIndex = 1;
            // 
            // btnDepart
            // 
            this.btnDepart.Location = new System.Drawing.Point(531, 24);
            this.btnDepart.Name = "btnDepart";
            this.btnDepart.Size = new System.Drawing.Size(89, 23);
            this.btnDepart.TabIndex = 2;
            this.btnDepart.Text = "AU DEPART";
            this.btnDepart.UseVisualStyleBackColor = true;
            this.btnDepart.Click += new System.EventHandler(this.btnDepart_Click);
            // 
            // FormAffichageCoureurs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 261);
            this.Controls.Add(this.btnDepart);
            this.Controls.Add(this.dataGVC);
            this.Controls.Add(this.cbEquipes);
            this.Name = "FormAffichageCoureurs";
            this.Text = "Affichage des Coureurs et Equipes en mode déconnecté";
            this.Load += new System.EventHandler(this.FormAffichageCoureurs_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGVC)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbEquipes;
        private System.Windows.Forms.DataGridView dataGVC;
        private System.Windows.Forms.Button btnDepart;
    }
}