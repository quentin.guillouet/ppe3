﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace PPE3_THT
{
 /// <summary>
 /// Classe Controleur qui gère le lien entre les vues et le MODELE
 /// </summary>
    public static class Controleur
    {
        #region propriétés
        private static Modele vmodele;
        #endregion

        #region accesseurs
        /// <summary>
        /// propriété Vmodele
        /// </summary>
        public static Modele Vmodele
        {
            get { return vmodele; }
            set { vmodele = value; }
        }
        #endregion

        #region methodes
        /// <summary>
        /// instanciation du modele
        /// </summary>
        public static void init()
        {
            Vmodele = new PPE3_THT.Modele();
        }

        /// <summary>
        /// Méthode de déconnexion à la BDD
        /// </summary>
        public static void Deconnexion()
        {
            if (Vmodele.Connopen)    // si connexion ouverte
            {
                Vmodele.sedeconnecter();
            }

        }
        public static void crud_relais(Char c, int indice)
        {
            // Le Char c correspond à l'action : c:create, u update, d delete,
            // la cle est celle de l'enregistrement sélectionné, vide si action d’ajout (c = ‘c’)

            FormCRUDPR formCRUD = new FormCRUDPR(c, indice); // création de la nouvelle forme
            if (c == 'c') // mode ajout donc pas de valeur à passer à la nouvelle forme
            {
                formCRUD.TbNom.Text = "";
                formCRUD.TbAdresse.Text = "";
                formCRUD.TbVille.Text = "";
                formCRUD.TbLong.Text = "";
                formCRUD.TbLat.Text = "";
                formCRUD.TbPoint.Text = "";
            }
            if (c == 'u' || c == 'd') // mode update ou delete donc on récupère les champs
            {

                // on remplit les zones par les valeurs du dataView correspondantes
                formCRUD.TbNom.Text = Vmodele.DT[1].Rows[indice][1].ToString();
                formCRUD.TbAdresse.Text = Vmodele.DT[1].Rows[indice][2].ToString();
                formCRUD.TbVille.Text = Vmodele.DT[1].Rows[indice][3].ToString();
                formCRUD.TbLong.Text = Vmodele.DT[1].Rows[indice][4].ToString();
                formCRUD.TbLat.Text = Vmodele.DT[1].Rows[indice][5].ToString();
                formCRUD.TbPoint.Text = Vmodele.DT[1].Rows[indice][6].ToString();
            }
            // on affiche la nouvelle form
            formCRUD.ShowDialog();
            // si l’utilisateur clique sur OK
            if (formCRUD.DialogResult == DialogResult.OK)
            {
                if (c == 'c') // ajout
                {
                    // on crée une nouvelle ligne dans le dataView
                    DataRow newRow = Vmodele.DT[1].NewRow();
                    if (formCRUD.TbNom.Text != "") { newRow["NOMPR"] = formCRUD.TbNom.Text; }
                    if (formCRUD.TbAdresse.Text != "") { newRow["ADRESSEPR"] = formCRUD.TbAdresse.Text; }
                    if (formCRUD.TbVille.Text != "") { newRow["VILLEPR"] = formCRUD.TbVille.Text; }
                    if (formCRUD.TbLong.Text != "") { newRow["LONGITUDEPR"] = Convert.ToDouble(formCRUD.TbLong.Text); }
                    if (formCRUD.TbLat.Text != "") { newRow["LATITUDEPR"] = Convert.ToDouble(formCRUD.TbLat.Text); }
                    if (formCRUD.TbPoint.Text != "") { newRow["POINTKILOPR"] = Convert.ToDouble(formCRUD.TbPoint.Text); }
                    Vmodele.DT[1].Rows.Add(newRow); // on ajoute cette ligne au dataTable : 
                    Vmodele.DA[1].Update(Vmodele.DT[1]);
                }
                if (c == 'u') // modif
                {
                    // on met à jour le dataView avec les nouvelles valeurs
                    if (formCRUD.TbNom.Text != "") { Vmodele.DT[1].Rows[indice]["NOMPR"] = formCRUD.TbNom.Text; }
                    if (formCRUD.TbAdresse.Text != "") { Vmodele.DT[1].Rows[indice]["ADRESSEPR"] = formCRUD.TbAdresse.Text; }
                    if (formCRUD.TbVille.Text != "") { Vmodele.DT[1].Rows[indice]["VILLEPR"] = formCRUD.TbVille.Text; }
                    if (formCRUD.TbLong.Text != "") { Vmodele.DT[1].Rows[indice]["LONGITUDEPR"] = Convert.ToDouble(formCRUD.TbLong.Text); }
                    if (formCRUD.TbLat.Text != "") { Vmodele.DT[1].Rows[indice]["LATITUDEPR"] = Convert.ToDouble(formCRUD.TbLat.Text); }
                    if (formCRUD.TbPoint.Text != "") { Vmodele.DT[1].Rows[indice]["POINTKILOPR"] = Convert.ToDouble(formCRUD.TbPoint.Text); }
                    Vmodele.DA[1].Update(Vmodele.DT[1]);
                }
                if (c == 'd') // suppression
                {
                    // on supprime l’élément du DataView
                    Vmodele.DT[1].Rows[indice].Delete();
                    Vmodele.DA[1].Update(Vmodele.DT[1]);
                }
                formCRUD.Dispose(); // on ferme la form
            }
            else
            {
                formCRUD.Dispose();
            }
        }
        public static void crud_passage(FormSaisiePassage fsp)
        {
            string numE = fsp.CBEquipe1.SelectedValue.ToString();
            int numEq = Convert.ToInt32(numE);
            string numP = fsp.CBPointRelais1.SelectedValue.ToString();
            int numPr = Convert.ToInt32(numP);
            string dateP = fsp.DTPDate1.Value.ToString();
            DateTime date1 = Convert.ToDateTime(dateP);
            DataRowView newRow = Controleur.Vmodele.Dv_passage.AddNew();
            newRow["IDEQUIPE"] = numEq;
            newRow["IDPR"] = numPr;
            newRow["JOURETHEURE"] = date1;
            newRow.EndEdit();
        }
        #endregion
    }
}
