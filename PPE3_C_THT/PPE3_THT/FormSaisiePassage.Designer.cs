﻿using System.Windows.Forms;

namespace PPE3_THT
{
    partial class FormSaisiePassage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnAnn = new System.Windows.Forms.Button();
            this.CBEquipe = new System.Windows.Forms.ComboBox();
            this.CBPointRelais = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.DTPDate = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(90, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID Equipe :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(90, 181);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "ID Point Relais :";
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(93, 457);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.BtnOK_Click);
            // 
            // btnAnn
            // 
            this.btnAnn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAnn.Location = new System.Drawing.Point(322, 456);
            this.btnAnn.Name = "btnAnn";
            this.btnAnn.Size = new System.Drawing.Size(75, 23);
            this.btnAnn.TabIndex = 5;
            this.btnAnn.Text = "ANNULER";
            this.btnAnn.UseVisualStyleBackColor = true;
            this.btnAnn.Click += new System.EventHandler(this.BtnAnn_Click);
            // 
            // CBEquipe
            // 
            this.CBEquipe.FormattingEnabled = true;
            this.CBEquipe.Location = new System.Drawing.Point(214, 87);
            this.CBEquipe.Name = "CBEquipe";
            this.CBEquipe.Size = new System.Drawing.Size(121, 21);
            this.CBEquipe.TabIndex = 6;
            // 
            // CBPointRelais
            // 
            this.CBPointRelais.FormattingEnabled = true;
            this.CBPointRelais.Location = new System.Drawing.Point(214, 181);
            this.CBPointRelais.Name = "CBPointRelais";
            this.CBPointRelais.Size = new System.Drawing.Size(121, 21);
            this.CBPointRelais.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(90, 275);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(170, 16);
            this.label3.TabIndex = 8;
            this.label3.Text = "Date et heure de passage :";
            // 
            // DTPDate
            // 
            this.DTPDate.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.DTPDate.Location = new System.Drawing.Point(281, 271);
            this.DTPDate.Name = "DTPDate";
            this.DTPDate.Size = new System.Drawing.Size(87, 20);
            this.DTPDate.TabIndex = 9;
            // 
            // FormSaisiePassage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(522, 541);
            this.Controls.Add(this.DTPDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.CBPointRelais);
            this.Controls.Add(this.CBEquipe);
            this.Controls.Add(this.btnAnn);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FormSaisiePassage";
            this.Text = "FormSaisiePassage";
            this.Load += new System.EventHandler(this.FormSaisiePassage_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnAnn;
        private ComboBox CBEquipe;
        private ComboBox CBPointRelais;
        private Label label3;
        private DateTimePicker DTPDate;

        public ComboBox CBEquipe1 { get => CBEquipe; set => CBEquipe = value; }
        public ComboBox CBPointRelais1 { get => CBPointRelais; set => CBPointRelais = value; }
        public DateTimePicker DTPDate1 { get => DTPDate; set => DTPDate = value; }
    }
}