<?php
require_once '../Class/Connexion.class.php';

class AssociationModele {

	private $idcASS = null;

	public function __construct() {
		// creation de la connexion afin d'executer les requetes
		try {
			$ConnexionASS = new Connexion();
			$this->idcASS = $ConnexionASS->IDconnexion;
		} catch ( PDOException $e ) {
			echo "<h1>probleme access BDD</h1>";
		}
	}
	public function add($nomASS, $causeASS){
		// ajoute cette association dans  la BDD
		$nb = 0;
		if ($this->idcASS) {
			$prep = $this->idcASS->prepare("INSERT INTO association(`NOMASS`,`CAUSEASS`) VALUES (:nom,:cause);");
            $prep->bindParam(":nom",$nomASS);
            $prep->bindParam(":cause",$causeASS);
		    $nb = $prep->execute();// si nb ==1 alors l'insertion s est bien passee
		return $nb; // si nb =1 alors l'insertion s est bien passee
	}
}
		
	public function getMaxID() {
		// recupere l'idmax de l'association qui vient d'être insérée
		if ($this->idcASS) {
			$req= "SELECT max(IDASS) as MAXIDASS from association";
			$resultID = $this->idcASS->query($req)->fetch()->MAXIDASS;
			return $resultID;
		}
	}
	public function getAssociationS() {
		// recupere TOUTES les Associations de  la BDD
		if ($this->idcASS) {
			$req ="SELECT * from association;" ;
			$resultASS = $this->idcASS->query($req);
			return $resultASS;
		}
	}
}
?>