<?php
require_once '../Class/Connexion.class.php';

class CoureurModele {

	private $idcCOU = null;

	public function __construct() {
		// creation de la connexion afin d'executer les requetes
		try {
			$ConnexionCOU = new Connexion();
			$this->idcCOU = $ConnexionCOU->IDconnexion;
		} catch ( PDOException $e ) {
			echo "<h1>probleme access BDD</h1>";
		}
	}
    public function add($idCou,$idEqu,$cm){
        // ajoute ce coureur dans  la BDD
		$nb = 0;
		if ($this->idcCOU) {
			
			$prep = $this->idcCOU->prepare("INSERT INTO coureur(IDCOU,IDEQU,CERTIFICATMEDCOU) VALUES (:idC, :idE, :cm)");
            $prep->bindParam(":idC",$idCou);
            $prep->bindParam(":idE",$idEqu);
            $prep->bindParam(":cm",$cm);
           
		    $nb = $prep->execute();// si nb ==1 alors l'insertion s est bien passee
		return $nb; // si nb =1 alors l'insertion s est bien passee
	}
}
      

	public function getCoureursParEquipe($idEQU) {
		// recupere TOUS les coureurs pour une EQUIPE passée en paramètre


	}
	
	//Pas possible de redefinir de la méthode getCoureurS en PHP
	public function compteCoureursParEquipe($idEQU) {
		// recupere TOUS les coureurs pour une EQUIPE passée en paramètre
		if ($this->idcCOU) {
			$req ="SELECT count(*) as CPTCOU from coureur c  WHERE IDEQU=".$idEQU.";" ;
			$resultEQU = $this->idcCOU->query($req)->fetch()->CPTCOU;
			return $resultEQU;
		}
	}
}
?>