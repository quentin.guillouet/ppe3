<?php
session_start();

require_once ('../Class/autoload.php');
require_once('../CONTROLEUR/controleurConsultation.php');

if (isset ( $_SESSION ['idU'] ) && isset ( $_SESSION ['mdpU'] )) {

	$pageConsultationEquipes = new PageSecurisee ("Consulter les équipes et leurs coureurs...");
} else {
	$pageConsultationEquipes = new PageBase ("Consulter les équipes et leurs coureurs...");
}

$listeEQU = listeEquipeAssociation(); //appel de la fonction dans le CONTROLEUR : page controleurConsultation.php

$pageConsultationEquipes->contenu = '<section>
					<div class="col-md-6">
          <table class="table table-striped">
            <thead>	<tr><th>Nom Equipe</th><th>son slogan</th><th>son Association</th></tr></thead><tbody>';
//parcours du résultat de la requete
foreach ($listeEQU as $unEQU){
					$pageConsultationEquipes->contenu .= '<tr><td>'.$unEQU->NOMEQU.'</td><td>'.$unEQU->SLOGANEQU.'</td><td><b>'.$unEQU->NOMASS.'</b><br/>'.$unEQU->CAUSEASS.'</td>
					<td><a href="consultationCoureurs.php?idE='.$unEQU->IDEQU.'"/>Ses coureurs...</a></td></tr>';
}
$listeEQU->closeCursor(); //pour liberer la memoire occupee par le resultat de la requete
$listeEQU = null; //pour une autre execution avec cette variable

$pageConsultationEquipes->contenu .= '</tbody></table></div>';

/*// TRAITEMENT du RETOUR DE L'ERREUR par le controleur
if (isset($_GET['error']) && !empty($_GET['error'])) {
	$err = $_GET['error'];
	$pageConsultationEquipes->zoneErreur = '<div id="infoERREUR" class="alert alert-success fade in"><strong>INFO : </strong><a href="#" onclick="cacher();" class="close" data-dismiss="alert">&times;</a></div>';
	$verif = preg_match("/ERREUR/",$err); //verifie s'il y a le mot erreur dans le message retourné
	if ( $verif == TRUE ){
		$class ="alert alert-danger fade in";
	}
	else {
		$class ="alert alert-success fade in";
	}
	$pageConsultationEquipes->scriptExec = "changerCouleurZoneErreur('".$class."');";	//ajout dans le tableau scriptExec du script à executer	
	$pageConsultationEquipes->scriptExec = "montrer('".$err."');"; //ajout dans le tableau scriptExec du script à executer
}*/
$pageConsultationEquipes->afficher();
?>