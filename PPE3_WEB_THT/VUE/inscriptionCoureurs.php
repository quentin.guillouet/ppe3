<?php
session_start ();

require_once ('../Class/autoload.php');
require_once('../CONTROLEUR/controleurConsultation.php');

if (isset ( $_SESSION ['idU'] ) && isset ( $_SESSION ['mdpU'] )) {
	$pageInscriptionCoureurs = new PageSecurisee ( "Ajouter un ou des nouveaux coureurs..." );
} else {
	$pageInscriptionCoureurs = new PageBase ( "Ajouter un ou des nouveaux coureurs..." );
}
$pageInscriptionCoureurs->contenu .= '<section>
<article>
<form class="form" id="formInscriptionCoureurs" method="GET" action="../VUE/inscriptionEquipeCoureurs.php">
<div class="form-group">
<h4>Choisir votre Equipe </h4>';
//sur le formulaire, on appelle la vue InscriptionEquipeCoureurs qui gère déjà les inscriptions des coureurs 
//mais on lui passe en GET l'équipe sélectionnée ci-dessous via les radiobuttons

$listeEqu = listeEquipeAssociation(); //appel d'une fonction du controleur

foreach ($listeEqu as $uneEqu){
		$pageInscriptionCoureurs->contenu .= '<label class="radio"><input type="radio" id='.$uneEqu->IDEQU.' value='.$uneEqu->IDEQU.' name="idE" required>'.$uneEqu->NOMEQU.'</label>';
}
			
$pageInscriptionCoureurs->contenu .='	<div class="form-group">
			<input type="submit" class="btn btn-default"  name="btnAjoutCoureur" value="Ajouter un Coureur"/></div>
			</form>
		</article> </section>';
				
$listeEqu->closeCursor (); // pour libérer la mémoire occupée par le résultat de la requéte
$listeEqu = null; // pour une autre exécution avec cette variable


				
// TRAITEMENT du RETOUR DE L'ERREUR par le controleur
if (isset($_GET['error']) && !empty($_GET['error'])) {
	$err = $_GET['error'];
	$pageInscriptionCoureurs->zoneErreur = '<div id="infoERREUR" class="alert alert-success fade in"><strong>INFO : </strong><a href="#" onclick="cacher();" class="close" data-dismiss="alert">&times;</a></div>';
	$verif = preg_match("/ERREUR/",$err); //verifie s'il y a le mot erreur dans le message retourné
	if ( $verif == TRUE ){
		$class ="alert alert-danger fade in";
	}
	else {
		$class ="alert alert-success fade in";
	}
	$pageInscriptionCoureurs->scriptExec = "changerCouleurZoneErreur('".$class."');";	//ajout dans le tableau scriptExec du script à executer	
	$pageInscriptionCoureurs->scriptExec = "montrer('".$err."');"; //ajout dans le tableau scriptExec du script à executer
}
$pageInscriptionCoureurs->afficher();
?>