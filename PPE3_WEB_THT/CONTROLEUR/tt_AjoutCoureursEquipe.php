<?php
require_once ('../MODELE/PersonneModele.class.php');
require_once ('../MODELE/CoureurModele.class.php');

$msgERREUR = "";

if (isset ($_POST ['nomCOU'] ) && isset ( $_POST ['prenomCOU'] )) {
	$modelePER = new PersonneModele();
	$modeleCOU = new CoureurModele();
	try {
		//récupération de l'ID de la dernière équipe insérée par le POST
		if (isset($_POST['idE']))  $IDE=$_POST['idE'];
		//echo "id de l'equipe : ".$IDE;
		
		//requête qui récupère le nombre de coureur inscrit pour une équipe
		$cptCOU = $modeleCOU->compteCoureursParEquipe($IDE);
		if ($cptCOU < 4) //4 étant le nombre maximum de coureurs par équipe
		{
			//requête permettant d'ajouter une personne
			$nbPER = $modelePER->add($_POST['nomCOU'],$_POST['prenomCOU'],$_POST['mailCOU']);
			
			//requête qui récupère l'ID de la dernière personne insérée
			$maxIdPer = $modelePER->getMaxIdPER();
			//echo "maxid : ".$maxIdPer;

			if ($nbPER==1) 	$msgERREUR .= "SUCCESS : AJOUT_d\'une_personne_avec_ID=".$maxIdPer."-";
			
			//requête permettant d'ajouter un coureur
			$nbCOU = $modeleCOU->add($maxIdPer, $IDE,0);
			if ($nbCOU==1) 	$msgERREUR .= "AJOUT_d\'un_coureur-";

			$cptCOU = $modeleCOU->compteCoureursParEquipe($IDE);
			if ($cptCOU == 4){
				$msgERREUR .= "BRAVO : Vous avez inscrit TOUS les coureurs de votre EQUIPE";
			}
			else{
				$nbCoureurManquant = 4 - $cptCOU;
				$msgERREUR .= "ERREUR : il vous manque ENCORE ".$nbCoureurManquant ." coureur(s) dans votre EQUIPE !";
			}
		}
		else{
			$msgERREUR .= "ERREUR : AJOUT IMPOSSIBLE : Il y a déjà 4 coureurs dans cette équipe !";
		}	
	} catch ( PDOException $pdoe ) {
		// erreur attrapée de MySQL
		$msgERREUR .= "ERREUR dans l\'ajout du coureur !  <br/>" . $pdoe->getMessage ();
	}
}
header ( 'Location: ../VUE/inscriptionEquipeCoureurs.php?error='.$msgERREUR.'&idE='.$IDE);