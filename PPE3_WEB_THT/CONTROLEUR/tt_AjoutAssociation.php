<?php
require_once ('../MODELE/AssociationModele.class.php');
$msgERREUR = "";
if (isset ( $_POST ['nomASS'] ) && isset ( $_POST ['causeASS'] ) ) {
	$modeleASS = new AssociationModele ();
	try {
		$nb = $modeleASS->add( $_POST ['nomASS'], $_POST ['causeASS']);
		$msgERREUR = "SUCCESS : AJOUT de votre association";		
	} catch ( PDOException $pdoe ) {
		// cas ou 2 pseudo ont deja mis un commentaire sur un jeu
		$msgERREUR = "ERREUR dans l'ajout de votre association ! : <br/>" . $pdoe->getMessage ();
	}
}
header('Location: ../VUE/ajoutAssociation.php?error='.$msgERREUR);
?>